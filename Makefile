SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c

export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1
export PROJECT=apps

targets: help

check-tools: ## Check and install package management dependencies
	pip install pip==23.2.1 pip-tools==7.3.0

compile: check-tools ## Check and compile project dependencies (check-tools will be performed automatically)
	pip-compile --strip-extras --resolver=backtracking requirements.in

upgrade-all-packages: ## Upgrades all dependency packages
	pip-compile --strip-extras --resolver=backtracking requirements.in --upgrade

install: check-tools ## Installs project dependencies (check-tools will be performed automatically)
	pip-sync requirements.txt

sync: compile install ## Synchronizes project dependencies (compile and install will be performed automatically)

black: ## Executes auto-formatting tool
	black apps

mypy: ## Checks python typing
	mypy apps --config-file mypy.ini

flake8: ## Lints project source code with flake8
	pflake8 apps

safety-check: ## Scans security issues with the dependency packages
	safety check -r requirements.txt --full-report

check: black flake8 mypy tests safety ## Runs all kinds of tests together

run-tests: run-test-deps tests

run-test-deps:
	export TEST_MODE=1;
	docker compose -f docker-compose.yaml up postgres -d;

tests:
	pytest apps

up: ## Run the application
	docker compose -f docker-compose.yaml up


ci-docker-compose := docker compose -f ./docker-compose.yaml

cleantest:  ## Clean up test containers
	$(ci-docker-compose) down -v --remove-orphans

help:
	@awk -F '##' '/^[a-z_\-]+:[a-z \-]+##/ { print "\033[34m"$$1"\033[0m" "\n" $$2 }' Makefile