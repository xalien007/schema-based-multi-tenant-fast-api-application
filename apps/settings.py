from functools import lru_cache
import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    TORTOISE_TEST_DB = "postgres://root:admin@127.0.0.1:5432/root"
    DATABASE_URL: str = os.getenv("DATABASE_URL", TORTOISE_TEST_DB)
    SECRET_KEY: str = "SuperSecret Key"
    APP_NAME: str = "Multi Tenant Service API"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 20160  # 14 Days
    TOKEN_ALGORITHM: str = "HS256"
    # SMTP_SERVER: str
    MAIL_SENDER: str = "noreply@example.com"
    API_PREFIX: str = "/api/v1"
    HOST: str = "localhost"
    PORT: str = 8000
    BASE_URL: str = "{}:{}/".format(HOST, str(PORT))
    MODELS: list[str] = [
        "apps.tenants.models", "aerich.models"
    ]
    TENANT_MODELS: list[str] = [
        "apps.users.models"
    ]
    TORTOISE_ORM_FOR_TEST: dict = {
        "connections": {
            "public": {
                "engine": "tortoise.backends.asyncpg",
                "credentials": {
                    "host": "localhost",
                    "port": "5432",
                    "user": "root",
                    "password": "admin",
                    "database": "testdb",
                }
            }
        },
        "apps": {
            "public": {
                "models": ["apps.tenants.models"], "default_connection": "public"
            }
        }
    }
    TEST_MODE: str = "0"


@lru_cache()
def get_settings():
    return Settings()
