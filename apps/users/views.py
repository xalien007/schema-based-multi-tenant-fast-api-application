from typing import List
from uuid import UUID

from fastapi import HTTPException, status, Depends
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from tortoise.exceptions import IntegrityError
from tortoise.transactions import in_transaction

from apps.core.security import get_tenant_name
from apps.users.schemas import (
    User,
    User_Pydantic,
    UserIn_Pydantic,
)

router = InferringRouter()


@cbv(router)
class UserViews:

    schema_name: str = Depends(get_tenant_name)

    @router.get("/users", response_model=List[User_Pydantic], tags=["user"])
    async def get_users(self):
        async with in_transaction(self.schema_name):
            return await User_Pydantic.from_queryset(User.all())

    @router.get("/users/{user_id}", response_model=User_Pydantic, tags=["user"])
    async def retrieve_user(self, user_id: UUID):
        async with in_transaction(self.schema_name):
            return await User_Pydantic.from_queryset_single(User.get(id=user_id))

    @router.delete("/users/{user_id}", status_code=status.HTTP_204_NO_CONTENT, tags=["user"])
    async def delete_user(self, user_id: UUID):
        async with in_transaction(self.schema_name):
            await User.filter(id=user_id).delete()
            return {}


@cbv(router)
class AuthViews:

    schema_name: str = Depends(get_tenant_name)

    @router.post("/auth/signup", response_model=User_Pydantic, status_code=status.HTTP_201_CREATED, tags=["auth"])
    async def sign_up(self, user: UserIn_Pydantic):
        async with in_transaction(self.schema_name):
            try:
                user_obj = await User.create(**user.dict(exclude_unset=True))
            except IntegrityError:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="User with this email already exists")
            return await User_Pydantic.from_tortoise_orm(user_obj)
