from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from apps.users.models import User, UserSettings


class ChangePassowrd(BaseModel):
    current_password: str
    new_password: str
    confirm_password: str


class SignIn(BaseModel):
    email: str
    password: str


User_Pydantic = pydantic_model_creator(User, name="User", exclude=("password",))
UserIn_Pydantic = pydantic_model_creator(
    User, name="UserIn", exclude_readonly=True, exclude=("status", )
)
UserSettings_Pydantic = pydantic_model_creator(UserSettings, name="UserSettings")
