import re

from tortoise import fields
from tortoise.validators import RegexValidator

from apps.core.models import AbstractUUIDBaseModel
from apps.users import StatusChoices


class User(AbstractUUIDBaseModel):
    email = fields.CharField(
        max_length=50, unique=True,
        validators=[RegexValidator("([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+", re.I)]
    )
    first_name = fields.CharField(max_length=100, null=True)
    last_name = fields.CharField(max_length=100, null=True)
    password = fields.CharField(max_length=128, null=False)
    date_joined = fields.DatetimeField(auto_now_add=True, use_tz=False, null=True)
    last_login_at = fields.DatetimeField(auto_now=True, use_tz=False, null=True)
    status = fields.CharEnumField(StatusChoices, default=StatusChoices.UNVERIFIED)

    class Meta:
        table = "user"
        ordering = ["-created_at"]


class UserSettings(AbstractUUIDBaseModel):
    user = fields.OneToOneField("tenants.User", on_delete=fields.CASCADE, related_name="settings")
    # allow_notification = fields.BooleanField(default=True)
    # is_verified = fields.BooleanField(default=True)

    class Meta:
        table = "user_settings"
        ordering = ["-created_at"]
