from apps.core.enums import TextChoices


class StatusChoices(TextChoices):
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"
    UNVERIFIED = "UNVERIFIED"
