from typing import List
from uuid import UUID

from fastapi import HTTPException, status
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from tortoise.exceptions import IntegrityError
from tortoise.transactions import in_transaction

from apps.tenants.schemas import (
    Tenant,
    TenantIn,
    Tenant_Pydantic
)
from apps.tenants.services import TenantMigrationService
from apps.users.models import User, UserSettings


router = InferringRouter()


@cbv(router)
class TenantViews:

    migration_service = TenantMigrationService()

    @router.post("/tenants", response_model=Tenant_Pydantic, status_code=status.HTTP_201_CREATED, tags=["tenant"])
    async def register_tenant(self, tenant: TenantIn):
        try:
            tenant_obj = await Tenant.create(**tenant.dict(exclude_unset=True))
        except IntegrityError:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Tenant with this name already exists")
        else:
            async with in_transaction(tenant_obj.tenant_schema_name):
                user_obj = await User.create(**{"email": tenant.email, "password": "Test1234!"})
                await UserSettings.create(**{"user": user_obj})
            await self.migration_service.upgrade_all_tenants(force_migrate=True, schema_name=tenant_obj.tenant_schema_name)
        return await Tenant_Pydantic.from_tortoise_orm(tenant_obj)

    @router.get("/tenants", response_model=List[Tenant_Pydantic], tags=["tenant"])
    async def get_tenants(self):
        return await Tenant_Pydantic.from_queryset(Tenant.all())

    @router.get("/tenants/upgrade-databases", status_code=status.HTTP_204_NO_CONTENT, tags=["migration"])
    async def upgrade_tenant_databases(self):
        await self.migration_service.upgrade_all_tenants()
        return {}

    @router.get("/tenants/downgrade-databases", status_code=status.HTTP_204_NO_CONTENT, tags=["migration"])
    async def downgrade_tenant_databases(self):
        await self.migration_service.downgrade_all_tenants()
        return {}

    @router.get("/tenants/{tenant_id}", response_model=Tenant_Pydantic, tags=["tenant"])
    async def retrieve_tenants(self, tenant_id: UUID):
        return await Tenant_Pydantic.from_queryset_single(Tenant.get(id=tenant_id))

    @router.delete("/tenants/{tenant_id}", status_code=status.HTTP_204_NO_CONTENT, tags=["tenant"])
    async def delete_tenant(self, tenant_id: UUID):
        await Tenant.filter(id=tenant_id).delete()
        return {}
