import logging
from typing import Type

from tortoise import BaseDBAsyncClient
from tortoise.signals import post_save

from apps.core.databases import initialise_tenant_connection_alias
from apps.tenants.models import Tenant

logging.basicConfig(level="INFO")

LOGGER = logging.getLogger(__name__)


@post_save(Tenant)
async def tenant_registered(
    sender: "Type[Tenant]", instance: Tenant, created: bool, using_db: BaseDBAsyncClient | None,
    update_fields: list[str]
) -> None:
    if created:
        LOGGER.info("Processing tenant registered post save signal.")
        await initialise_tenant_connection_alias(instance.tenant_schema_name)
        LOGGER.info("Successfully processed tenant registered post save signal.")
