from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from apps.tenants.models import Tenant


class TenantIn(BaseModel):
    email: str
    name: str
    host_name: str


Tenant_Pydantic = pydantic_model_creator(Tenant, name="Tenant")
