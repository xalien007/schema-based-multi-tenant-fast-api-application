
from aerich.models import Aerich
from tortoise.models import Model

from apps.core.databases import get_tenant_db_config, CustomCommand
from apps.tenants.models import Tenant


class TenantMigrationService:

    def __init__(
            self, tenant_model: Model = Tenant, app_name: str = "tenants", migration_path: str = "apps/migrations"
    ) -> None:
        self.model = tenant_model
        self._app_name = app_name
        self._location = migration_path

    async def upgrade_all_tenants(self, run_in_transaction: bool = True, force_migrate: bool = False, schema_name: str = None) -> None:
        tenant_schema_names = await self.model.all().values_list("tenant_schema_name", flat=True)
        total_tenants = len(tenant_schema_names)
        last_version_idx = 0
        if latest_migrated_aerich := await Aerich.filter(app=self._app_name).first():
            last_version_idx = latest_migrated_aerich.id

        if force_migrate and schema_name:
            tenant_schema_names = [schema_name]

        for schema_name in tenant_schema_names:
            tenant_db_config = get_tenant_db_config(schema_name)
            command = CustomCommand(tenant_db_config, self._app_name, self._location)
            await command.init()
            await command.upgrade(run_in_transaction, total_tenants, last_version_idx, force_migrate)

        return

    async def downgrade_all_tenants(self, version: int = -1, force_delete: bool = False) -> None:
        for tenant in await self.model.all():
            schema_name = tenant.tenant_schema_name
            tenant_db_config = get_tenant_db_config(schema_name)
            command = CustomCommand(tenant_db_config, self._app_name, self._location)
            await command.init()
            await command.downgrade(version=version, delete=force_delete)
        return
