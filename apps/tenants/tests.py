import pytest
from tortoise import Tortoise
from tortoise.contrib.test import IsolatedTestCase

from apps.tenants.models import Tenant


class TenantModelTest(IsolatedTestCase):

    tortoise_test_modules = ["apps.tenants.models"]

    async def _setUpDB(self) -> None:

        config = {
            "connections": {
                "public": {
                    "engine": "tortoise.backends.asyncpg",
                    "credentials": {
                        "host": "localhost",
                        "port": "5432",
                        "user": "root",
                        "password": "admin",
                        "database": "testdb",
                    }
                }
            },
            "apps": {
                "public": {
                    "models": ["apps.tenants.models"], "default_connection": "public"
                }
            }
        }
        await Tortoise.init(config, _create_db=True)
        await Tortoise.generate_schemas(safe=True)

    @pytest.mark.anyio
    async def test_tenant_model_add(self):
        tenant_obj = await Tenant.create(name="Test Company", host_name="test.me.com")
        self.assertEqual(tenant_obj.name, "Test Company")
        self.assertEqual(tenant_obj.tenant_schema_name, "test_company")
