from typing import Iterable

from tortoise import fields
from tortoise.backends.base.client import BaseDBAsyncClient

from apps.core.models import AbstractUUIDBaseModel


class Tenant(AbstractUUIDBaseModel):
    name = fields.CharField(max_length=150)
    host_name = fields.CharField(max_length=100)
    tenant_schema_name = fields.CharField(max_length=200, null=True)
    db_url = fields.CharField(max_length=200, null=True)

    class Meta:
        table = "tenant"
        ordering = ["-created_at"]
        unique_together = ("host_name", "tenant_schema_name")

    async def save(
            self, using_db: BaseDBAsyncClient | None = None, update_fields: Iterable[str] | None = None,
            force_create: bool = False, force_update: bool = False
    ) -> None:
        if name := self.name:
            self.tenant_schema_name = "_".join(name.lower().strip().split(" "))
            update_fields = ["tenant_schema_name"]
        return await super().save(using_db, update_fields, force_create, force_update)
