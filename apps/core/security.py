from fastapi import HTTPException, status
from starlette.requests import Request

from apps.core.databases import tenant_connection_alias
from apps.settings import get_settings
from apps.tenants.models import Tenant

settings = get_settings()


async def get_tenant_name(request: Request) -> str:
    host_name = request.headers.get("X-HOST")
    if host_name is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="No host found in header",
            headers={"X-Host": None},
        )

    if tenant := await Tenant.get_or_none(host_name=host_name):
        await tenant_connection_alias(tenant.tenant_schema_name)
        return tenant.tenant_schema_name

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="Tenant not found with the specified domain"
    )
