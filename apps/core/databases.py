import os
from pathlib import Path

from aerich import Command
from aerich.exceptions import DowngradeError
from aerich.migrate import (
    Migrate,

)
from aerich.models import Aerich
from aerich.utils import (
    get_app_connection_name,
    get_models_describe,
    import_py_file,
)
from fastapi import FastAPI
from tortoise import Tortoise
from tortoise.contrib.fastapi import register_tortoise
from tortoise.exceptions import OperationalError
from tortoise.transactions import in_transaction

from apps.settings import get_settings

settings = get_settings()


TORTOISE_ORM = {
    "connections": {
        "public": settings.DATABASE_URL.split("?")[0],
        "tenants": settings.DATABASE_URL.split("?")[0]
    },
    "apps": {
        "public": {
            "models": settings.MODELS,
            "default_connection": "public",
        },
        "tenants": {
            "models": settings.TENANT_MODELS,
            "default_connection": "tenants",
        },
    },
}


class CustomCommand(Command):

    async def _upgrade(self, conn, version_file, force_migrate):
        file_path = Path(Migrate.migrate_location, version_file)
        m = import_py_file(file_path)
        upgrade = getattr(m, "upgrade")
        try:
            await conn.execute_script(await upgrade(conn))
        except OperationalError:
            if force_migrate:
                await Aerich.create(
                    version=version_file,
                    app=self.app,
                    content=get_models_describe(self.app),
                )
        else:
            await Aerich.create(
                version=version_file,
                app=self.app,
                content=get_models_describe(self.app),
            )

    async def upgrade(self, run_in_transaction: bool, total_tenants: int, last_version_idx: int = None, force_migrate: bool = False):
        migrated = []
        for version_file in Migrate.get_all_version_files():
            if not version_file.endswith("init.py"):
                total_aerich_migration_versions = await Aerich.exclude(id__lte=last_version_idx).filter(
                    version=version_file, app=self.app
                ).count()
                if total_aerich_migration_versions < total_tenants:
                    app_conn_name = get_app_connection_name(self.tortoise_config, self.app)
                    if run_in_transaction:
                        async with in_transaction(app_conn_name) as conn:
                            await self._upgrade(conn, version_file, force_migrate)
                    migrated.append(version_file)
        return migrated

    async def downgrade(self, version: int, delete: bool):
        ret = []
        if version == -1:
            specified_version = await Migrate.get_last_version()
        else:
            specified_version = await Aerich.filter(
                app=self.app, version__startswith=f"{version}_"
            ).first()
        if not specified_version:
            raise DowngradeError("No specified version found")
        if version == -1:
            versions = [specified_version]
        else:
            versions = await Aerich.filter(app=self.app, pk__gte=specified_version.pk).order_by("-version")
        for version in versions:
            file = version.version
            async with in_transaction(
                get_app_connection_name(self.tortoise_config, self.app)
            ) as conn:
                file_path = Path(Migrate.migrate_location, file)
                m = import_py_file(file_path)
                downgrade = getattr(m, "downgrade")
                downgrade_sql = await downgrade(conn)
                if not downgrade_sql.strip():
                    raise DowngradeError("No downgrade items found")
                await conn.execute_script(downgrade_sql)
                await version.delete()
                if delete:
                    os.unlink(file_path)
                ret.append(file)
        return ret


def init_db(app: FastAPI) -> None:
    config = TORTOISE_ORM
    if bool(os.getenv("TEST_MODE") == "1"):
        config = settings.TORTOISE_ORM_FOR_TEST

    register_tortoise(
        app,
        config=config,
        generate_schemas=True,
        add_exception_handlers=True,
    )


def get_tenant_db_config(schema_name: str) -> dict:
    return {
            "connections": {
                    schema_name: f"{settings.DATABASE_URL.split('?')[0]}?schema={schema_name}",
            },
            "apps": {
                "tenants": {
                    "models": settings.TENANT_MODELS,
                    "default_connection": schema_name,
                }
            },
        }


async def initialise_tenant_connection_alias(schema_name: str) -> None:
    await Tortoise.init(config=get_tenant_db_config(schema_name))
    async with in_transaction(schema_name) as conn:
        await conn.execute_query(f"create schema {schema_name};")
        await conn.execute_query(f"set search_path to {schema_name};")
        await Tortoise.generate_schemas()
    return


async def tenant_connection_alias(schema_name: str) -> None:
    await Tortoise.init(config=get_tenant_db_config(schema_name))
    return
