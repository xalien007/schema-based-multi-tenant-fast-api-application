
from tortoise import fields
from tortoise.models import Model


class AbstractUUIDBaseModel(Model):
    id = fields.UUIDField(pk=True)
    created_at = fields.DatetimeField(auto_now_add=True, use_tz=False, null=True)
    created_by = fields.UUIDField(null=True)
    updated_at = fields.DatetimeField(auto_now=True, use_tz=False, null=True)
    updated_by = fields.UUIDField(null=True)

    class Meta:
        abstract = True
